<!-- @formatter:off -->
# Changelog

All notable changes to this project will be documented in this file.

## [%0.1.0] - 2022-08-14

### 🔨 Miscellaneous Tasks

- [[`9b1ddb6e`](https://gitlab.com/midnight-theme/micro/-/commit/9b1ddb6ef842281a8b1460d3be7af95ba99e6143)] Initial commit

<!-- @formatter:on -->
